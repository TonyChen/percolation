/****************************************************************************
 *  Author:         Tony Chen
 *  Written:        2014-06-19
 *  Last updated:   2015-07-03
 *
 *  Compilation:    javac Percolation.java
 *  Execution:      java Percolation
 *
 *  Percolation class.
 *
 ***************************************************************************/

/**
 *
 *  For additional documentation, see
 *  <a href="http://algs4.cs.princeton.edu/15uf">Section 1.5</a> of
 *  <i>Algorithms, 4th Edition</i> by Robert Sedgewick and Kevin Wayne.
 *
 */

public class Percolation {
    private static final byte SITE_BLOCKED    = 0;
    private static final byte SITE_OPEN       = 1;
    private static final byte SITE_TO_TOP     = 2;
    private static final byte SITE_TO_BOTTOM  = 4;
    private static final byte SITE_PERCOLATED = (SITE_TO_TOP | SITE_TO_BOTTOM);

    private int sizeN;                  // site for N x N grid
    private byte[] sites;               // N x N for status
    private WeightedQuickUnionUF uf;    // N x N for union-find
    private boolean percolated;

    // create N-by-N grid, with all sites blocked
    public Percolation(int N) {
        if (N <= 0) {
            throw new java.lang.IllegalArgumentException("N = "+N);
        }

        int totalSize = N * N;
        sizeN = N;

        uf = new WeightedQuickUnionUF(totalSize);

        sites = new byte[totalSize];
        for (int i = 0; i < totalSize; i++) {
            sites[i] = SITE_BLOCKED;
        }

        percolated = false;
    }

    private byte union(int currPos, int offset, byte status) {
        byte result = status;
        int nextPos = currPos + offset;
        if (sites[nextPos] != SITE_BLOCKED) {
            if (!isStatusPercolated(status)) {
                int root = uf.find(nextPos);
                if (result != sites[root]) {
                    result |= sites[root];
                    sites[root] = result;
                    if (root != nextPos) sites[nextPos] = result;
                }
            }
            uf.union(nextPos, currPos);
        }
        return result;
    }

    private boolean isStatusPercolated(byte status) {
        return (status & SITE_PERCOLATED) == SITE_PERCOLATED;
    }

    // open site (row i, column j) if it is not already
    public void open(int i, int j) {
        int pos = toOffset(i, j);
        if (sites[pos] == SITE_BLOCKED) {
            byte status = SITE_OPEN;

            if (i == 1) status |= SITE_TO_TOP;
            if (i == sizeN) status |= SITE_TO_BOTTOM;

            // check up site
            if (i > 1) status = union(pos, -sizeN, status);

            // check down site
            if (i < sizeN) status = union(pos, sizeN, status);

            // check left site
            if (j > 1) status = union(pos, -1, status);

            // check right site
            if (j < sizeN) status = union(pos, 1, status);

            sites[pos] = status;
            int root = uf.find(pos);
            sites[root] = status;

            if (isStatusPercolated(status)) percolated = true;
        }
    }

    // is site (row i, column j) open?
    public boolean isOpen(int i, int j) {
        int offset = toOffset(i, j);
        return sites[offset] != SITE_BLOCKED;
    }

    // is site (row i, column j) full?
    public boolean isFull(int i, int j) {
        int offset = toOffset(i, j);
        if (sites[offset] == SITE_BLOCKED) return false;
        if (i == 1 || (sites[offset] & SITE_TO_TOP) != 0) return true;

        int root = uf.find(offset);
        byte status = sites[root];
        if (root != offset) sites[offset] = status;
        return (status & SITE_TO_TOP) != 0;
    }

    // does the system percolate?
    public boolean percolates() {
        return percolated;
    }

    // convert (row i, column j: 1-based) to offset of sites[]
    private int toOffset(int i, int j) {
        if (i < 1 || i > sizeN || j < 1 || j > sizeN) {
            throw new java.lang.IndexOutOfBoundsException("i="+i+", j="+j);
        }
        return (i - 1) * sizeN + (j - 1);
    }

    private String siteStatus(byte status) {
        if (status == SITE_BLOCKED) return "-";
        if (isStatusPercolated(status)) return "P";
        if ((status & SITE_TO_TOP) != 0) return "T";
        if ((status & SITE_TO_BOTTOM) != 0) return "B";
        return "O";
    }

    private void printSites() {
        StdOut.println("\npercolates() is "+percolates());
        int offset = 0;
        for (int i = 0; i < sizeN; i++) {
            StdOut.println("");
            for (int j = 0; j < sizeN; j++) {
                String s = siteStatus(sites[offset]);
                if (s.equals("-") || uf.find(offset) != offset) {
                    StdOut.print(" "+s+" ");
                } else {
                    StdOut.print("["+s+"]");
                }
                offset++;
            }
        }
        StdOut.println("\n");
    }

    public static void main(String[] args) {
        byte[] x = { 0, 1, 0, 0, 1,     // 0:blocked, 1:open but no percolate
                     1, 1, 0, 0, 1,     // 2:percolated (no more than once)
                     1, 0, 1, 0, 2,
                     0, 1, 0, 1, 1,
                     0, 1, 0, 1, 0
                    };
        int width = (int) Math.sqrt(x.length);
        int last = -1;
        boolean error = false;
        Percolation perc = new Percolation(width);

        if (perc.percolates()) {
            StdOut.println("error: initial percolates() is true.");
            error = true;
        }

        for (int n = 0; n < x.length; n++) {
            int i = n / width + 1;
            int j = n % width + 1;

            if (last >= 0 && x[n] >= 2) x[n] = 1;
            switch(x[n]) {
                case 0:
                    StdOut.print(" - ");
                    break;
                case 1:
                    StdOut.print(" X ");
                    perc.open(i, j);
                    if (!perc.isOpen(i, j)) {
                        StdOut.println("error: isOpen("+i+","+j+") is false.");
                        error = true;
                    }
                    if (perc.percolates()) {
                        StdOut.println("error: percolates() is true.");
                        error = true;
                    }
                    break;
                default:
                    StdOut.print(" ? ");
                    last = n;
                    break;
            }

            if (j == width) StdOut.println("");
        }

        if (last > 0) {
            int i = last / width + 1;
            int j = last % width + 1;

            StdOut.println("open("+i+","+j+") which marks as \"?\"");
            perc.open(i, j);

            if (!perc.isOpen(i, j)) {
                StdOut.println("error: isOpen("+i+","+j+") is false.");
                error = true;
            }
            if (!perc.percolates()) {
                StdOut.println("error: percolates() is false.");
                error = true;
            }
        }

        // output last content of sites[]
        perc.printSites();

        if (!error) {
            StdOut.println("test passed.");
        }
    }
}