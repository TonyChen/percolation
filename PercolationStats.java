/****************************************************************************
 *  Author:         Tony Chen
 *  Written:        2014-06-21
 *  Last updated:   2015-07-03
 *
 *  Compilation:    javac PercolationStats.java
 *  Execution:      java PercolationStats N T
 *
 *  Monte Carlo simulation. To estimate the percolation threshold (N x N size),
 *  By repeating this computation experiment T times and averaging the results.
 *
 ***************************************************************************/

public class PercolationStats {

    private double fMean;
    private double fStddev;
    private double fPercent95;

    // perform T independent computational experiments on an N-by-N grid
    public PercolationStats(int N, int T) {
        if (N <= 0 || T <= 0) {
            throw new java.lang.IllegalArgumentException("N="+N+", T="+T);
        }

        // SD = σ = SQRT(Σ(x-μ)^2 / (T-1))
        // σ^2 = Σx^2 / (T-1) - (Σx)^2 / (T (T-1)) = (Σx^2 - μ Σx) / (T-1)
        /*-*/ double sumxx = 0;
        /*-*/ double sumx = 0;

        double[] x = new double[T];
        for (int i = 0; i < T; i++) {
            x[i] = calcPercolationThreshold(N);
            // StdOut.println("["+i+"] = "+x[i]);
            /*-*/ sumx += x[i];
            /*-*/ sumxx += x[i] * x[i];
        }

        /*-*/ double xMean = sumx / T;
        /*-*/ double xStddev = Double.NaN;
        /*-*/ if (T > 1) xStddev = Math.sqrt((sumxx-xMean*sumx) / (T-1));

        fMean = StdStats.mean(x);
        fStddev = StdStats.stddev(x);
        fPercent95 = 1.96 * fStddev / Math.sqrt(T);

        if (Math.abs(xMean-fMean)/fMean > 1e-12) {
            StdOut.println("fMean:"+fMean+", xMean:"+xMean);
        }
        if (Math.abs(xStddev-fStddev) > 1e-12) {
            StdOut.println("fStddev:"+fStddev+", xStddev:"+xStddev);
        }
    }

    // perform one computational experiments on an N-by-N grid
    // return percolation threshold
    private double calcPercolationThreshold(int N) {
        // offsets[] = 0..(N^2 - 1)
        int totalSites = N * N;
        int[] offsets = new int[totalSites];
        for (int i = 0; i < totalSites; i++) {
            offsets[i] = i;
        }

        Percolation perc = new Percolation(N);
        int count = 0;
        do {
            // random choose a value from offsets[count..totalSites-1]
            // and exchange it with offsets[count]
            // so offsets[] always keep each one for (0~totalSize-1)
            int k = StdRandom.uniform(totalSites-count) + count;
            int v = offsets[k];
            if (k != count) {
                offsets[k] = offsets[count];
                offsets[count] = v;
            }

            // v = 0 ~ N^2 - 1 (totalSize-1)
            int i = v / N + 1;
            int j = v % N + 1;
            if (perc.isOpen(i, j)) {
                throw new java.lang.RuntimeException(
                    "perc.site["+i+","+j+"] already opened.");
            }
            perc.open(i, j);
            count++;

        } while (!perc.percolates());

        return (double) count / totalSites;
    }

    // sample mean of percolation threshold
    public double mean() {
        return fMean;
    }

    // sample standard deviation of percolation threshold
    public double stddev() {
        return fStddev;
    }

    // returns lower bound of the 95% confidence interval
    public double confidenceLo() {
        return fMean - fPercent95;
    }

    // returns upper bound of the 95% confidence interval
    public double confidenceHi() {
        return fMean + fPercent95;
    }

    // test client, described below
    public static void main(String[] args) {
        int N = Integer.parseInt(args[0]);
        int T = Integer.parseInt(args[1]);

        // Stopwatch time = new Stopwatch();
        PercolationStats percStats = new PercolationStats(N, T);
        // StdOut.println("--> elapsed " + time.elapsedTime() + " seconds");

        StdOut.println("mean                    = " + percStats.mean());
        StdOut.println("stddev                  = " + percStats.stddev());
        StdOut.println("95% confidence interval = "
            + percStats.confidenceLo() + " , " + percStats.confidenceHi());
        StdOut.println("");
    }

}