# Makefile
# vim: set noet ts=8 sw=8:

SOURCE=Percolation.java PercolationStats.java
TARGET=$(SOURCE:.java=.class) \
	WeightedQuickUnionUF.class PercolationVisualizer.class

.PHONY: all compile clean testall test test10 stats stats2 help \
	checkstyle findbugs zip

%.class: %.java
	@/bin/echo -n "% "
	javac-algs4 $<

compile: $(TARGET)
	@echo "Compiling done."

all: testall checkstyle findbugs zip
	@echo "All done."

help:
	@echo "usage: make [ALL|checkstyle|findbugs|zip|test|test10|stats|stats2]"

clean:
	rm -f *.class *.zip

zip: percolation.zip

percolation.zip: $(SOURCE)
	@/bin/echo -n "% "
	zip -u $@ $^

checkstyle: $(SOURCE)
	@/bin/echo -n "% "
	checkstyle-algs4 $^

findbugs: $(SOURCE:.java=.class)
	@/bin/echo -n "% "
	findbugs-algs4 $^

testall: test test10 stats stats2

test: Percolation.class WeightedQuickUnionUF.class
	@/bin/echo -n "% "
	java-algs4 Percolation

test10: PercolationVisualizer.class Percolation.class WeightedQuickUnionUF.class
	@/bin/echo -n "% "
	java-algs4 PercolationVisualizer data/input10-no.txt 0
	@/bin/echo -n "% "
	java-algs4 PercolationVisualizer data/input10.txt 0

stats: PercolationStats.class Percolation.class WeightedQuickUnionUF.class
	@/bin/echo -n "% "
	java-algs4 PercolationStats 200 100
	@/bin/echo -n "% "
	java-algs4 PercolationStats 200 100

stats2: PercolationStats.class Percolation.class WeightedQuickUnionUF.class
	@/bin/echo -n "% "
	java-algs4 PercolationStats 2 1000
	@/bin/echo -n "% "
	java-algs4 PercolationStats 2 10000

